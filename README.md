# NLP - Find similar NeurIPS papers

As machine learning engineers, we occasionally stumble across a exiting paper and we might want to look for other similar papers. Given this corpus of [NeurIPS paper abstracts](https://radix-senior-hiring-challenge.s3.eu-west-1.amazonaws.com/nlp/neurips_papers.zip), can you build a system that allows to retrieve similar papers given the abstract of another paper?

# Solution

The objective is to build a recommendation system that allows to retrieve similar scientific papers given the title and abstract of another paper.

For this purpose, a bipartite document-topic graph is created where the topics of each document are obtained from an API (https://www.semanticscholar.org/product/api). Using this bipartite graph, it is possible to produce a document-level graph where an edge exists between two documents if they have at least one topic in common.

This last graph is used to mine triplets documents: an anchor document, a positive document and a negative document. The anchor and the positive example are two documents connected by an edge and the negative sample is either sampled randomly from the graph or is a document connected to the positive document but not the anchor document (hard negative).

The title and abstract of those triplets are then used in order to fine-tune a SciBERT model (https://arxiv.org/abs/1903.10676) using the triplet margin loss function in order to encode the relationship between documents into the representation. Using this representation, it is possible to retrieve similar scientific papers using the Euclidian distance as similarity function.

# Local setup

A docker image is provided to reproduce the environment. Please run the following to set it up:

 > git clone https://gitlab.com/amin.najji/neurips

 > cd neurips

 > docker build -t neurips .
 
 > docker run -p 8888:8888 neurips

 Once the docker image is runing, please navigate to http://localhost:8888/ and log in using the token listed in your terminal.

